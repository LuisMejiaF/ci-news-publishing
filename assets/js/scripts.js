$(function() {

    /**
     * Handling the behavior of the image preview for the article creation
     */
    $("#image").on("change", function(){
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader)
            return; // no file selected, or no FileReader support  
            
        if (/^image/.test( files[0].type)){ // only image file
            var reader = new FileReader(); // instance of the FileReader
            reader.readAsDataURL(files[0]); // read the local file
 
            reader.onloadend = function(){ // set image data as background of div
                $("#imagePreview").css("background-image", "url("+this.result+")");
            }
        }
    });

    /**
     * Verifying that the user has selected an image for the article creation
     */
    $("#create_post_form").on('submit', function(e){
        var files = !!$("#image")[0].files ? $("#image")[0].files : [];

        if (!files.length || !window.FileReader){
            alert('Please select an image');

            return false; // no file selected, or no FileReader support  
        } 

        if ($('#image')[0].files[0].type != 'image/jpeg'){
            alert('Just JPEG images are allowed');
            return false;
        }
    });

    /**
     * This is to confirm that the user is sure about deleting the article that he/she just clicked
     */
    $('.delete-article-btn').on('click', function(e){
        var r = confirm("Are you sure you want to delete this entry?");

        if (r != true)
            return false;
    });
});