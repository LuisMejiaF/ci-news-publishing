-- Dumping database structure for news_web_app_db
CREATE DATABASE IF NOT EXISTS `news_web_app_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `news_web_app_db`;

-- Dumping structure for table news_web_app_db.news
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(250) NOT NULL,
  `uri` varchar(250) NOT NULL,
  `author` int(11) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `body` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uri` (`uri`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping structure for table news_web_app_db.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `user_email` varchar(150) NOT NULL,
  `user_pass` varchar(70) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email_confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `confirmation_token` varchar(250) NOT NULL,
  `user_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Adding the foreign keys
ALTER TABLE `news`
ADD CONSTRAINT `FK_news_users` 
FOREIGN KEY (`author`) 
REFERENCES `users` (`user_id`);