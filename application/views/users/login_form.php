<?php
	if (!isset($user_identifier)) $user_identifier = '';
?>


<h2>Login</h2>

<?= form_open('login'); ?>
	<div class="small-centered-content">
	
		<?php 
			if (isset($error)) { 
				switch ($error) {
					case 'email_need_confirmation':
						$error_msg = 'Please check your email for a confirmation link before trying to log in.';
						break;
					
					case 'wrong_credentials':
						$error_msg = 'The information provided to log in is incorrect!';
						break;
					
					default:
						$error_msg = 'Something went wrong :(';
						break;
				}
				
					?>
						<div class="alert alert-danger" role="alert"><?= $error_msg ?></div>
					
		<?php } ?>

		<?= validation_errors(); ?>
		
		<div class="form-group">
			<label for="user_identifier">Username or Email</label>
			<input type="text" class="form-control" name="user_identifier" id="user_identifier" placeholder="Username or Email" value="<?= $user_identifier ?>">
		</div>
		
		<div class="form-group">
			<label for="password">Password</label>
			<input type="password" class="form-control" name="password" id="password" placeholder="Password">
		</div>


	 	<button type="submit" class="btn btn-default">Submit</button>

	 	<span>Do not have an account yet? Create one <a href="<?= base_url('register') ?>">here</a>.</span>
	</div>

	<input type="hidden" name="form_submitted" value="1"> 
</form>