<h2>Account confirmation</h2>

<?= form_open("account_confirmation/$confirmation_token"); ?>
	<div class="small-centered-content">
	
		<?php if (isset($error)) { ?>
			<div class="alert alert-danger" role="alert">The password is incorrect!</div>
		<?php } ?>

		<?= validation_errors(); ?>
		
		<div class="form-group">
			<label for="password">Please enter the password associated with your email address</label>
			<input type="password" class="form-control" name="password" id="password" placeholder="Password">
		</div>


	 	<button type="submit" class="btn btn-default">Submit</button>
	</div>

	<input type="hidden" name="form_submitted" value="1"> 
</form>