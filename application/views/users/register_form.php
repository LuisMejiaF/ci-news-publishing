<?php
	
	if (!isset($first_name)) $first_name = '';
	if (!isset($last_name)) $last_name = '';
	if (!isset($username)) $username = '';
	if (!isset($email)) $email = '';
	if (!isset($password)) $password = '';
	if (!isset($cn_password)) $cn_password = '';
	
?>


<h2>Register</h2>

<?= form_open('register'); ?>
	<div class="small-centered-content">

		<?php 
			if (isset($error)) { 
				switch ($error) {
					case 'email_exists':
						$error_msg = "The email <b>$email</b> is already is use.";
						break;
					
					case 'username_exists':
						$error_msg = "The username <b>$username</b> is already is use.";
						break;

					case 'unknown':
						$error_msg = 'Ooops! Something happened while processing the info. Please try again.';
						break;

					case 'wrong_input':
						$error_msg = 'Please correct the fields indicated bellow';
						break;
					
					default:
						$error_msg = 'Something went wrong :(';
						break;
				}
				
					?>
						<div class="alert alert-danger" role="alert"><?= $error_msg ?></div>
					
		<?php } ?>

		<?= validation_errors(); ?>

		<div class="form-group">
			<label for="first_name">First Name</label>
			<input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" value="<?= $first_name ?>">
		</div>

		<div class="form-group">
			<label for="last_name">Last Name</label>
			<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" value="<?= $last_name ?>">
		</div>

		<div class="form-group">
			<label for="username">Username</label>
			<input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?= $username ?>">
		</div>

		<div class="form-group">
			<label for="email">Email</label>
			<input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<?= $email ?>">
		</div>

		<div class="form-group">
			<label for="password">Password</label>
			<input type="password" class="form-control" name="password" id="password" placeholder="Password">
		</div>

		<div class="form-group">
			<label for="cn_password">Confirm Password</label>
			<input type="password" class="form-control" name="cn_password" id="cn_password" placeholder="Confirm Password">
		</div>

	  	<button type="submit" class="btn btn-default">Submit</button>
	</div>

	<input type="hidden" name="form_submitted" value="1"> 
</form>