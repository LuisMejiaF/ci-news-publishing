<div class="small-centered-content">
	<p>Thanks for registering <?= ucfirst($this->session->userdata('first_name')) ?> :) </p>
	<p>You can now browse and share news of <a href="<?= base_url() ?>">our site</a>.</p>
	<p>To post news, please check your email inbox to confirm your account. <br>Once you have confirmed your account, you will be able to post news by yourself.</p>
</div>