<h2>
	<?= $article->title ?><br>
	<small style="font-size: 40%;">
		Created at <?= date('F j, Y - g:i a', strtotime($article->created)) ?>, by <?= $article->first_name . ' ' . $article->last_name ?>
	</small>	
</h2>

<div class="medium-centered-content">
	<hr>
	
	<?php if ($this->session->flashdata('new_post_created') != NULL) { ?>
		<div class="alert alert-success" role="alert">Your post has been created!</div>
	<?php } ?>

	<center><img class="article-image-view" src="<?= base_url("assets/images/{$article->image}") ?>"></center>

	<?=  strip_tags(str_replace('</p>', '<br><br>', $article->body), '<br>') ?>
</div>