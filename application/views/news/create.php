<?php
	if (!isset($title)) $title = '';
	if (!isset($content)) $content = '';
?>


<h2>Post a new entry</h2>


<?php if ($isEmailConfirmed) { ?>

	<script src="<?= base_url() ?>assets/js/tinymce/tinymce.min.js"></script>

	<?= form_open_multipart('create', array('id' => 'create_post_form')); ?>
		<div class="medium-centered-content">
		
			<?php 
				if (isset($error)) { 
					switch ($error) {
						default:
							$error_msg = 'Something went wrong. Please try again.';
							break;
					}		
			?>
				<div class="alert alert-danger" role="alert"><?= $error_msg ?></div>
						
			<?php } ?>

			<?= validation_errors(); ?>
			
			<div class="form-group">
				<label for="title">Title</label>
				<input type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?= $title ?>">
			</div>

			<div class="form-group">
				<label for="content">Post Content</label>
				<textarea id="content" name="content" class="form-control" rows="3"><?= $content ?></textarea>
			</div>
			
			<div class="form-group">
				<label for="image">Image</label>
				<input type="file" class="form-control" name="image" id="image" placeholder="image">
			</div>
			<p>
				<div id="imagePreview"></div>
			</p>


		 	<button type="submit" class="btn btn-default">Create</button>
		</div>

		<input type="hidden" name="form_submitted" value="1"> 
	</form>
<?php } else { ?>
	<div class="small-centered-content">
		<p>You have not confirmed your email address yet. Please check your inbox (or maybe the spam folder), 
			and open the link in the email. <br><br>Once you email address is confirmed, you will be able to post news.</p>
	</div>
<?php } ?>


<script type="text/javascript">
	tinymce.init({
	    selector: "#content"
	});
</script>
