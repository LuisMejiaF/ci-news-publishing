<h2>Latest News</h2>

<hr>

<div class="medium-centered-content">
	<?php if (count($articles) > 0) { ?>
		<?php foreach ($articles as $article) { ?>
			<?php
				$create = date('F j, Y - g:i a', strtotime($article->created));
				$link = base_url("article/{$article->uri}");
			?>

			<div class="quick-view-article">
				<h4>
					<a href="<?= $link ?>"><?= $article->title ?></a> 
					<small style="font-size: 55%;"><?= date('F j, Y - g:i a', strtotime($article->created)) ?>, by <?= $article->first_name . ' ' . $article->last_name ?></small>
					<br>
					<small><?= substr(strip_tags($article->body), 0, 255); ?>... <a href="<?= $link ?>">continue reading</a></small>
				</h4>
			</div>
		<?php } ?>

	<?php } else { ?>
		<p>No one has post any news in our site yet. The good news is that you can post/report news by yourself. </p>
		<p>Create a news entry <a href="<?= base_url('create') ?>">here</a>.</p>
	<?php } ?>
</div>
