<h2>
	<?= $article->title ?><br>
	<small style="font-size: 40%;">
		Created at <?= date('F j, Y - g:i a', strtotime($article->created)) ?>, by <?= $article->first_name . ' ' . $article->last_name ?>
	</small>	
</h2>

<div class="medium-centered-content">

	<p style="text-align:right">
		<a href="<?= base_url("download/{$article->uri}") ?>"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span> Download as PDF</a>
	</p>

	<hr>
	
	<?php if ($this->session->flashdata('new_post_created') != NULL) { ?>
		<div class="alert alert-success" role="alert">Your post has been created!</div>
	<?php } ?>

	<center><img class="article-image-view" src="<?= base_url("assets/images/{$article->image}") ?>"></center>

	<?= $article->body ?>
</div>