<h2>My Published News</h2>

<hr>

<div class="medium-centered-content">

	<p><a href="<?= base_url('create') ?>"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Post a new entry</a></p>

	<hr>

	<?php if ($this->session->flashdata('new_post_deleted') != NULL) { ?>
		<div class="alert alert-success" role="alert">The post has been deleted</div>
	<?php } ?>

	<?php if (count($articles) > 0) { ?>

		<table>

			<?php foreach ($articles as $article) { ?>
				<?php
					$create = date('F j, Y - g:i a', strtotime($article->created));
					$link = base_url("article/{$article->uri}");
				?>

				<tr>
					<td style="padding-right:40px">
						<div class="quick-view-article">
							<h4>
								<a href="<?= $link ?>"><?= $article->title ?></a> 
								<small style="font-size: 55%;"><?= date('F j, Y - g:i a', strtotime($article->created)) ?>, by <?= $article->first_name . ' ' . $article->last_name ?></small>
								<br>
								<small><?= substr(strip_tags($article->body), 0, 255); ?>... <a href="<?= $link ?>">continue reading</a></small>
							</h4>
						</div>
					</td>
					<td>
						<a class="delete-article-btn" href="<?= base_url("delete/{$article->id}") ?>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>
						<a href="<?= base_url("article/{$article->uri}") ?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
					</td>
				</tr>
			<?php } ?>

		</table>

	<?php } else { ?>
		<p>You have not posted any article yet.</p>
		<p>Create a news entry <a href="<?= base_url('create') ?>">here</a>.</p>
	<?php } ?>
</div>

<?php
	
?>