<?php
	$logged_in = is_logged();
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>News Site - by Luis Mejia F.</title>

    <meta name="description" content="Source code generated using layoutit.com">
    <meta name="author" content="LayoutIt!">

    <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>assets/css/style.css" rel="stylesheet">

    <script src="<?= base_url() ?>assets/js/jquery.min.js"></script>
    <script src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url() ?>assets/js/scripts.js"></script>

  </head>
  <body>

    <div id="main-container" class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<nav class="navbar navbar-default" role="navigation">
					<div class="navbar-header">
						 
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							 <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
						</button> <a class="navbar-brand" href="<?= base_url() ?>">Home</a>
					</div>
					
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						
						<ul class="nav navbar-nav navbar-right">

							<?php if($logged_in) { ?>


								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Hello <?= ucfirst($this->session->userdata('first_name')) ?></b><strong class="caret"></strong></a>
									<ul class="dropdown-menu">
										<li>
											<a href="<?= base_url('my_articles') ?>"><span class="glyphicon glyphicon-list-alt" aria-hidden="true"></span> My Published News</a>
										</li>
										<li>
											<a href="<?= base_url('create') ?>"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Post a new entry</a>
										</li>
										<li class="divider">
										</li>
										<li>
											<a href="<?= base_url('logout') ?>"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Logout</a>
										</li>
									</ul>
								</li>

							<?php } else { ?>
								<li><a href="<?= base_url('login') ?>"><span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Login</a></li>
							<?php } ?>
							
							
						</ul>
						
					
					</div>
					
				</nav>
			</div>
			