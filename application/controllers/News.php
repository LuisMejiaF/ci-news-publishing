<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller
{
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('News_model', 'news_model');
    }
    
    /**
     * Homepage of the application. Here we display the latest news, limited to 10.
     */
    public function index() {
        
        $articles = $this->news_model->getLatestNews(10);
        
        $view_data = array('articles' => $articles);
        
        $this->load->view('header');
        $this->load->view('news/latest', $view_data);
        $this->load->view('footer');
    }
    
    /**
     * Page where we create a news post entry into the DB
     */
    public function create() {
        
        check_logged();
        
        extract($_POST, EXTR_OVERWRITE);
        
        $this->load->model('Users_model', 'users_model');
        
        $isEmailConfirmed = $this->users_model->isEmailConfirmed(logged_user_id());
        
        $view_data = array('isEmailConfirmed' => $isEmailConfirmed);
        
        if (isset($form_submitted) && $form_submitted) {
            
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('title', 'Title', 'trim|required|min_length[5]|max_length[250]');
            $this->form_validation->set_rules('content', 'Post Content', 'trim|required|min_length[20]|max_length[40000]');
            
            $view_data['title'] = $title;
            $view_data['content'] = $content;
            
            if ($this->form_validation->run() == FALSE) {
                $view_data['error'] = TRUE;
            } 
            else {
                
                $res = $this->news_model->addNew();
                
                if ($res['result'] == TRUE) {
                    redirect("article/{$res['uri']}");
                } 
                else {
                    $view_data['error'] = $res['error_type'];
                }
            }
        }
        
        $this->load->view('header');
        
        $this->load->view('news/create', $view_data);
        
        $this->load->view('footer');
    }
    
    /**
     * Page where we delete a post, given its ID
     * @param  integer $article_id The article/post ID
     */
    public function delete($article_id) {
        $this->news_model->deleteNew($article_id);
        
        redirect('my_articles');
    }
    
    /**
     * Page where we can see an given post/article
     * @param  string $uri The URI of the post
     */
    public function view($uri = '') {
        if ($uri == '') show_404();
        
        $view_data = array();
        
        $article = $this->news_model->getNewByUri($uri);
        
        if (!isset($article->id)) show_404();
        
        $view_data['article'] = $article;
        
        $this->load->view('header');
        
        $this->load->view('news/view', $view_data);
        
        $this->load->view('footer');
    }
    
    /**
     * Page where the logged user can see its created posts
     */
    public function my_news() {
        check_logged();
        
        $view_data = array();
        
        $articles = $this->news_model->getNewsByAuthor(logged_user_id());
        
        $view_data['articles'] = $articles;
        
        $this->load->view('header');
        
        $this->load->view('news/my_news', $view_data);
        
        $this->load->view('footer');
    }
    
    /**
     * Page where the user can download an news article in PDF format
     * @param  string $uri The URI of the post/article
     */
    public function download($uri) {
        
        $article = $this->news_model->getNewByUri($uri);
        
        if (!isset($article->id)) show_404();
        
        $view_data = array('article' => $article);
        
        $pdf_body = $this->load->view('news/pdf_file', $view_data, TRUE);
        
        require_once (APPPATH . 'third_party/tcpdf/tcpdf.php');
        
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('TCPDF Example 001');
        $pdf->SetSubject('TCPDF Tutorial');
        
        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage();
        
        // Print text using writeHTMLCell()
        $pdf->writeHTMLCell(0, 0, '', '', $pdf_body, 0, 1, 0, true, '', true);
        
        // ---------------------------------------------------------
        
        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $pdf->Output('example_001.pdf', 'D');
    }
}
