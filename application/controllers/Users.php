<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller
{
    
    public function __construct() {
        parent::__construct();
        
        $this->load->model('Users_model', 'users_model');
    }
    
    /**
     * Page where the user can register
     */
    public function register() {
        if (is_logged()) redirect('/');
        
        extract($_POST, EXTR_OVERWRITE);
        
        $view_data = array();
        
        if (isset($form_submitted) && $form_submitted) {
            
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('first_name', 'First Name', 'trim|required|min_length[4]');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|min_length[4]');
            $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[4]');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[4]|max_length[32]');
            $this->form_validation->set_rules('cn_password', 'Password Confirmation', 'trim|required|matches[password]');
            
            $view_data['first_name'] = $first_name;
            $view_data['last_name'] = $last_name;
            $view_data['username'] = $username;
            $view_data['email'] = $email;
            $view_data['password'] = $password;
            
            if ($this->form_validation->run() == FALSE) {
                $view_data['error'] = 'wrong_input';
            } 
            else {
                $res = $this->users_model->addUser();
                
                if ($res['result'] === TRUE) redirect('/registered');
                else $view_data['error'] = $res['error_type'];
            }
        }
        
        $this->load->view('header');
        
        $this->load->view('users/register_form', $view_data);
        
        $this->load->view('footer');
    }
    
    /**
     * Confirmation page saying that the user's account was created successfuly. 
     */
    public function registered() {
        
        if (is_logged() && $this->session->flashdata('just_registered') == NULL) redirect('/');
        
        $view_data = array();
        
        $this->load->view('header');
        
        $this->load->view('users/registered', $view_data);
        
        $this->load->view('footer');
    }
    
    /**
     * Page where the user log in
     */
    public function login() {
        
        if (is_logged()) redirect('/');
        
        extract($_POST, EXTR_OVERWRITE);
        
        $view_data = array();
        
        if (isset($form_submitted) && $form_submitted) {
            
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('user_identifier', 'Username or Email', 'trim|required|min_length[4]');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[32]');
            
            $view_data['user_identifier'] = $user_identifier;
            
            if ($this->form_validation->run() == FALSE) {
            } 
            else {
                $url_to_redirect = get_url_to();
                
                $login_result = $this->users_model->login($user_identifier, $password);
                
                if ($login_result['result'] === TRUE) {
                    
                    redirect($url_to_redirect);
                } 
                else {
                    
                    $view_data['error'] = $login_result['error_type'];
                }
            }
        }
        
        $this->load->view('header');
        
        $this->load->view('users/login_form', $view_data);
        
        $this->load->view('footer');
    }
    	
    /**
     * Page where the user log out. The session is destroy.
     */
    public function logout() {
        $this->users_model->logout();
        
        redirect('/');
    }
    
    /**
     * Page where the user confirms its account
     * @param  string $confirmation_token The confirmation token that was send to the user's email address.
     */
    public function account_confirmation($confirmation_token = '') {
        
        extract($_POST, EXTR_OVERWRITE);
        
        $user_id = $this->users_model->getUserIdByConfirmationToken($confirmation_token);
        
        if (!$user_id) show_404();
        
        if (is_logged() && $user_id != logged_user_id()) show_404();
        
        $view_data = array();
        
        if (isset($form_submitted) && $form_submitted) {
            
            $this->load->library('form_validation');
            
            $this->form_validation->set_rules('password', 'Password', 'trim|required|max_length[32]');
            
            if ($this->form_validation->run() == FALSE) {
                
                $view_data['error'] = TRUE;
            } 
            else {
                $user_info = $this->users_model->getUserInfo($user_id);
                
                require_once (APPPATH . 'third_party/PasswordHash.php');
                
                $hasher = new PasswordHash(PHPASS_HASH_STRENGTH, PHPASS_HASH_PORTABLE);
                
                if ($hasher->CheckPassword($password, $user_info->user_pass)) {
                    $this->users_model->confirmEmail($user_id);
                    redirect('/account_confirmed');
                } 
                else $view_data['error'] = TRUE;
            }
        }
        
        $view_data['confirmation_token'] = $confirmation_token;
        
        $this->load->view('header');
        
        $this->load->view('users/account_confirmation', $view_data);
        
        $this->load->view('footer');
    }
  	
  	/**
  	 * Page that says to the users that its account has been confirmed.
  	 */
    public function account_confirmed() {
        if ($this->session->flashdata('account_is_now_confirmed') == NULL) show_404();
        
        $this->load->view('header');
        
        $this->load->view('users/account_confirmed');
        
        $this->load->view('footer');
    }

}
