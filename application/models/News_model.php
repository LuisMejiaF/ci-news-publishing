<?php
class News_model extends CI_Model
{
    
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * Returns an array containing the latest news that were created in the site
     * @param  integer $limit The limit of the result.
     * @return array
     */
    public function getLatestNews($limit = 20) {
        
        $this->db->select('*');
        $this->db->from('news');
        $this->db->join('users', 'users.user_id = news.author');
        $this->db->limit($limit);
        $this->db->order_by('created DESC');
        
        $res = $this->db->get();
        
        return $res->result();
    }
    
    /**
     * This function creates a new entry in the news table in the DB
     * @return array    An array containing the result of the operation
     */
    public function AddNew() {
        
        extract($_POST, EXTR_OVERWRITE);
        
        $temp_path = $_FILES["image"]["tmp_name"];
        $image_name = $_FILES["image"]["name"];
        $to_path = str_replace('application', 'assets', APPPATH) . 'images/' . $image_name;
        
        $moveResult = move_uploaded_file($temp_path, $to_path);
        
        // Evaluate the value returned from the function if needed
        if ($moveResult != true) return array('result' => FALSE, 'error_type' => 'image_upload_failed');
        
        $uri = $this->getUniqueUri($title);
        
        $data = array('title' => $title, 'uri' => $uri, 'author' => logged_user_id(), 'image' => $_FILES['image']['name'], 'body' => $content);
        
        $this->db->insert('news', $data);
        
        $this->session->set_flashdata('new_post_created', 1);
        
        return array('result' => TRUE, 'uri' => $uri);
    }
    
    /**
     * Deletes a news article
     * @param  string $article_id The article's ID to delete
     */
    public function deleteNew($article_id) {
        $this->db->where('id', $article_id);
        $this->db->delete('news');
        
        $this->session->set_flashdata('new_post_deleted', 1);
    }
    
    /**
     * Creates a unique URI slug that will serve as a unique URL for the created article
     * @param  string $title The title of the article
     * @return string        The URI created
     */
    public function getUniqueUri($title) {
        $uri = url_title($title);
        
        $i = 0;
        
        while ($this->_doesUriExists($uri)) {
            $uri = "$uri-$i";
            $i++;
        }
        
        return strtolower($uri);
    }
    
    /**
     * Returns an article object
     * @param  string $uri The URI of the article to look for
     * @return object      An article object
     */
    public function getNewByUri($uri) {
        $this->db->select('*');
        $this->db->from('news');
        $this->db->join('users', 'users.user_id = news.author');
        $this->db->where('uri', $uri);
        $res = $this->db->get();
        
        return $res->first_row();
    }
    
    /**
     * Returns a set of articles that has been created by the given user
     * @param  integer $user_id The user ID that has created the articles that we want to see
     * @return array            An array containing the articles
     */
    public function getNewsByAuthor($user_id) {
        $this->db->select('*');
        $this->db->from('news');
        $this->db->join('users', 'users.user_id = news.author');
        $this->db->where('user_id', $user_id);
        $res = $this->db->get();
        
        return $res->result();
    }
    
    /**
     * Verify is the given URI already exists or not in the DB
     * @access private
     * @param  string $uri The URI to look for
     * @return boolean
     */
    private function _doesUriExists($uri) {
        $this->db->where('uri', $uri);
        $res = $this->db->get('news');
        
        if ($res->num_rows() > 0) return TRUE;
        
        return FALSE;
    }
}
