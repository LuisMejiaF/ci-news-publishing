<?php
require APPPATH . 'third_party/PHPMailer/PHPMailerAutoload.php';

class Users_model extends CI_Model
{
    
    public function __construct() {
        parent::__construct();
        $this->load->library('SimpleLoginSecure');
    }
    
    /**
     * Creates a new user in the Database
     * @return array An array containing the result of the operation
     */
    public function addUser() {
        
        extract($_POST, EXTR_OVERWRITE);
        
        if ($this->_emailExists($email)) return array('result' => FALSE, 'error_type' => 'email_exists');
        
        if ($this->_usernameExists($username)) return array('result' => FALSE, 'error_type' => 'username_exists');
        
        if ($this->simpleloginsecure->create($email, $password, FALSE)) {
            
            $confirmation_token = md5(uniqid(rand()));
            
            $data = array('first_name' => $first_name, 'last_name' => $last_name, 'username' => $username, 'confirmation_token' => $confirmation_token);
            
            $this->db->where('user_id', $this->getUserIdByEmail($email));
            $this->db->update('users', $data);
            
            $this->simpleloginsecure->login($email, $password);
            
            $this->_sendConfirmationEmail($first_name, $email, $confirmation_token);
            
            $this->session->set_flashdata('just_registered', 1);
            
            return array('result' => TRUE, 'confirmation_token' => $confirmation_token);
        }
        
        return array('result' => FALSE, 'error_type' => 'unknown');
    }
    
    /**
     * Returns the User ID given the user's email
     * @param  string $email The email of the user to look for
     * @return integer       The User ID
     */
    public function getUserIdByEmail($email = '') {
        $this->db->select('user_id');
        $this->db->where('user_email', $email);
        $res = $this->db->get('users');
        
        if ($res->num_rows() > 0) return $res->first_row()->user_id;
        
        return 0;
    }

    /**
     * Returns the User ID givn a username
     * @param  string $username The Username to look for
     * @return integer          The User ID
     */
    public function getUserIdByUsername($username = '') {
        $this->db->select('user_id');
        $this->db->where('username', $username);
        $res = $this->db->get('users');
        
        if ($res->num_rows() > 0) return $res->first_row()->user_id;
        
        return 0;
    }
    
    /**
     * Returns the email address of the given user
     * @param  integer $user_id The User ID to look for
     * @return string           The user's email address
     */
    public function getEmailByUserId($user_id) {
        $this->db->select('user_email');
        $this->db->where('user_id', $user_id);
        $res = $this->db->get('users');
        
        if ($res->num_rows() > 0) return $res->first_row()->user_email;
        
        return '';
    }
    
    /**
     * Returns the User ID given its confirmation token
     * @param  string $confirmation_token The confirmation token sent to the user's email address
     * @return integer                    The User ID
     */
    public function getUserIdByConfirmationToken($confirmation_token) {
        $this->db->select('user_id');
        $this->db->where('confirmation_token', $confirmation_token);
        $res = $this->db->get('users');
        
        if ($res->num_rows() > 0) return $res->first_row()->user_id;
        
        return 0;
    }
    
    /**
     * Returns an user object, containing all its information
     * @param  integer $user_id The User ID to look for
     * @return object           An user object
     */
    public function getUserInfo($user_id) {
        $this->db->where('user_id', $user_id);
        $res = $this->db->get('users');
        
        return $res->first_row();
    }
    
    /**
     * Checks if the account of the given user has been confirmed or not
     * @param  integer  $user_id The User ID to look for
     * @return boolean          
     */
    public function isEmailConfirmed($user_id) {
        $this->db->select('email_confirmed');
        $this->db->where('user_id', $user_id);
        $res = $this->db->get('users');
        
        if ($res->num_rows() > 0) return (int)$res->first_row()->email_confirmed;
    }
    
    /**
     * This function confirms the account of the user (updates the DB information)
     * @param  integer $user_id The User ID to confirm
     */
    public function confirmEmail($user_id) {
        $this->db->set('confirmation_token', '');
        $this->db->set('email_confirmed', 1);
        $this->db->where('user_id', $user_id);
        
        $this->db->update('users');
        
        $this->session->set_flashdata('account_is_now_confirmed', 1);
    }
    
    /**
     * This functions creates a session for a given user
     * @param  string  $user_identifier Email or UserName of the user
     * @param  string  $password        The password of the user
     * @param  boolean $login_after     If set to TRUE, it will creates session. FALSE means that this function just verifies if the
     *                                  credentials provided are correct.
     * @return array                    An array containing the result of the operation
     */
    public function login($user_identifier, $password, $login_after = FALSE) {
        
        $email = '';
        
        if ($this->getUserIdByEmail($user_identifier)) {
            $email = $user_identifier;
        } 
        else {
            $user_id = $this->getUserIdByUsername($user_identifier);
            
            if ($user_id > 0) $email = $this->getEmailByUserId($user_id);
        }
        
        if ($this->simpleloginsecure->login($email, $password, $login_after)) return array('result' => TRUE);
        
        return array('result' => FALSE, 'error_type' => 'wrong_credentials');
    }
    
    /**
     * Logouts the user. Destroys the current session.
     */
    public function logout() {
        $this->simpleloginsecure->logout();
    }
    
    /**
     * Sends a email to the user's email address, containing a link to confirm its account.
     * @return private
     * @param  string $first_name         The user's first name
     * @param  string $email              The user's email address
     * @param  string $confirmation_token The confirmation token to send to the user
     */
    private function _sendConfirmationEmail($first_name, $email, $confirmation_token) {
        $mail = new PHPMailer;
        
        $mail->isSMTP(); // Set mailer to use SMTP
        $mail->Host = 'smtp.mandrillapp.com'; // Specify main and backup SMTP servers
        $mail->SMTPAuth = true; // Enable SMTP authentication
        $mail->Username = 'luismiguelsolfa@gmail.com'; // SMTP username
        $mail->Password = 'syux2ZghxOtY7ZRJs05vcA'; // SMTP password
        $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587; // TCP port to connect to
        
        $mail->setFrom('hola@luismejiaf.com', 'Luis Mejia from News Publishing Site');
        $mail->addAddress($email, $first_name);
        
        $mail->isHTML(true); // Set email format to HTML
        
        $link = base_url() . 'account_confirmation/' . $confirmation_token;
        
        $mail->Subject = 'Please active your account';
        $mail->Body = "$first_name, please click this <a href=\"$link\">link</a> to active your account.";
        $mail->AltBody = 'THanks for registering to our site.';
        
        $mail->send();
    }
    
    /**
     * Checks if the given email address is already exists or not in the database
     * @param  string $email The email address to look for
     * @return boolean
     */
    private function _emailExists($email = '') {
        $this->db->where('user_email', $email);
        $res = $this->db->get('users');
        
        if ($res->num_rows() > 0) return TRUE;
        
        return FALSE;
    }

    /**
     * Checks if the given username is alrady is user or not
     * @param  string $username The username to look for
     * @return boolean
     */
    private function _usernameExists($username = '') {
        $this->db->where('username', $username);
        $res = $this->db->get('users');
        
        if ($res->num_rows() > 0) return TRUE;
        
        return FALSE;
    }
}
