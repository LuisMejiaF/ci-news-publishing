<?php

/**
 * Function that tell us if the current visitor is a logged user
 * @return boolean
 */
function is_logged() {
    $CI = & get_instance();
    
    if ($CI->session->userdata('logged_in') != NULL) return $CI->session->userdata('logged_in');
    
    return FALSE;
}

/**
 * This function returns the User ID of the current logged user. If there is not logged user, it returns 0;
 * @return integer The logged User ID
 */
function logged_user_id() {
    $CI = & get_instance();
    
    if ($CI->session->userdata('user_id') != NULL) return (int)$CI->session->userdata('user_id');
    
    return 0;
}

/**
 * This function checks if the user is logged, and if NOT, it redirects the user to the login page.
 * The purpuse of this function is to provide security to those pages that we don't want to allow 
 * to see without a valid session.
 */
function check_logged() {
    if (!is_logged()) {
        $CI = & get_instance();
        $CI->session->set_flashdata('url_to', uri_string());
        
        redirect('login');
    }
}

/**
 * This function returns the URL where the user should be redirected after he/she just login
 * @return string the redirect URL
 */
function get_url_to() {
    $CI = & get_instance();
    
    if ($CI->session->flashdata('url_to') != NULL) return $CI->session->flashdata('url_to');
    
    return '/';
}
